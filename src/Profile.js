import React, { Component } from "react";
import { Dropdown, Table, Navbar } from 'react-bootstrap';
import { ChevronDown, ChevronLeft, User, Briefcase, Phone, Mail } from 'react-feather';

export default class Profile extends Component{
  render() {
    return (
      <div className="main-wrapper">
        <div className="loader">
          <img className="loader-img" src={require('./images/loader1.gif')} />
        </div>
        <header class="topbar topbar-full">
          <Navbar>
            <Navbar.Brand href="#home">
              <img className="client-logo" src={require('./images/client-logo.png')} />
            </Navbar.Brand>
            <div className="nav-right">
              <Dropdown>
                <Dropdown.Toggle id="dropdown-basic">
                  <ChevronDown color='#000' size="18" />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#">My Profile</Dropdown.Item>
                  <Dropdown.Item href="#" className="border-bottom">Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </Navbar>
        </header>
        <div class="page-wrapper page-full">
          <div className="profile-section">
            <div className="profile-top">
              <h2><ChevronLeft color='#000' size="24" /> My Profile</h2>
            </div>
            <div className="profile-details">
              <div className="profile-img">
                <img src={require('./images/profile.jpg')} />
              </div>
              <div className="profile-data">
                <div className="profile-data1">
                  <p><User color='#FF6347' size="20" /> Pramod Mohod </p>
                  <p><Briefcase color='#FF6347' size="18" /> Admin </p>
                </div>
                <div className="profile-data2">
                <p><Phone color='#FF6347' size="20" /> 987562341 </p>
                <p><Mail color='#FF6347' size="18" /> pramod.mohod@clideg.com </p>
                </div>
              </div>
            </div>
            <div className="project-list">
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Service</th>
                    <th>Projects</th>
                    <th>Module</th>
                    <th>My Reports</th>
                    <th>Module</th>
                    <th>My Reports</th>
                    <th>Module</th>
                    <th>My Reports</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td rowSpan="2">WEST 1</td>
                    <td>Malad</td>
                    <td>UA/UC</td>
                    <td>1</td>
                    <td>NM</td>
                    <td>1</td>
                    <td>IRA</td>
                    <td>1</td>
                  </tr>
                  <tr>
                    <td>Ghatkopar</td>
                    <td>UA/UC</td>
                    <td>1</td>
                    <td>NM</td>
                    <td>1</td>
                    <td>IRA</td>
                    <td>1</td>
                  </tr>
                  <tr>
                    <td rowSpan="3">WEST 2</td>
                    <td>Malad</td>
                    <td>UA/UC</td>
                    <td>1</td>
                    <td>NM</td>
                    <td>1</td>
                    <td>IRA</td>
                    <td>1</td>
                  </tr>
                  <tr>
                    <td>Ghatkopar</td>
                    <td>UA/UC</td>
                    <td>1</td>
                    <td>NM</td>
                    <td>1</td>
                    <td>IRA</td>
                    <td>1</td>
                  </tr>
                  <tr>
                    <td>Mahape</td>
                    <td>UA/UC</td>
                    <td>1</td>
                    <td>NM</td>
                    <td>1</td>
                    <td>IRA</td>
                    <td>1</td>
                  </tr>
                </tbody>
              </Table>
            </div>
          </div>
        </div>
      </div>
    )
  }
}