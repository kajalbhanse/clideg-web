import React from "react";
import Header from './Header';
import MainContent from './MainContent';
import Cart from './Cart';
import ListPhoto from './ListPhoto';
import Footer from './Footer';
import FormDisplay from './FormDisplay';
import Login from './Login';
import Dashboard from './SocialMedia/Dashboard';
import UaUcDashboard from './UaUc/Dashboard';
import Profile from './Profile';

function Website() {
	return(
		<div className="wrapper">
			{/* <Login /> */}
			<Dashboard />
			{/* <Profile /> */}
			{/* <UaUcDashboard /> */}
			{/* <Header />
			<FormDisplay />
			<MainContent />
			<ListPhoto />
			<Cart />
			<Footer />  */}
		</div>
	);
}

export default Website;