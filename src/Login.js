import React, { Component } from "react";
import LoginLeft from './component/LoginLeft';
import LoginRight from './component/LoginRight';

export default class Login extends Component{
  render() {
    return (
      <div className="login-wrapper">
        <div className="login-left">
          <LoginLeft />
        </div>
        <div className="login-right">
          <LoginRight />
        </div>
        <div className="login-footer">
          <img className="footer-img" src={require('./images/login-footer.png')} />
          <p className="footer-text">Powered by ClideG</p>
        </div>
      </div>
    )
  }
}