import React, { Component } from "react";
import { Navbar, Dropdown } from 'react-bootstrap';
import { ChevronDown } from 'react-feather';
import ReportingActivity from './../component/ReportingActivity';

export default class Dashboard extends Component{
  render() {
    return (
      <div className="main-wrapper">
        <header class="topbar">
          <Navbar>
            <Navbar.Brand href="#home">
              <img className="client-logo" src={require('./../images/client-logo.png')} />
            </Navbar.Brand>
            <div className="nav-right">
              <Dropdown>
                <Dropdown.Toggle id="dropdown-basic">
                  <ChevronDown color='#000' size="18" />
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item href="#">My Profile</Dropdown.Item>
                  <Dropdown.Item href="#" className="border-bottom">Logout</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </Navbar>
        </header>
        <aside class="left-sidebar">
          <div className="profile-side">
            <img src={require('./../images/profile.jpg')} />
            <h4>Aniket Kumbhare</h4>
            <h6>ADMIN</h6>
          </div>
          <div className="sidenav">
            <ul>
              <li>
                <a href="#">
                  <img src={require('./../images/side/ua-uc.png')} />
                  <span>UA/UC</span> 
                </a>
              </li>
              <li>
                <a href="#">
                  <img src={require('./../images/side/nm.png')} />
                  <span>NM</span> 
                </a>
              </li>
              <li>
                <a href="#">
                  <img src={require('./../images/side/ira.png')} />
                  <span>IRA</span> 
                </a>
              </li>
              <li href="#">
                <a>
                  <img src={require('./../images/side/tbt.png')} />
                  <span>TBT</span> 
                </a>
              </li>
            </ul>
            <div className="powered">
              <h6>Powered by ClideG</h6>
            </div>
          </div>
        </aside>
        <div class="page-wrapper">
          <div className="report-section">
            <ReportingActivity />
          </div>
        </div>
      </div>
    )
  }
}