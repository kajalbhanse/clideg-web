import React, { Component } from "react";
import Button from './Button';

export default class Pricing extends Component{
  render() {
    return (
      <div className="card mb-4 shadow-sm">
        <div className="card-header">
          <h4 className="my-0 font-weight-normal">{ this.props.title }</h4>
        </div>
        <div className="card-body">
          <h1 className="card-title pricing-card-title">{ this.props.charge } <small className="text-muted">/ mo</small></h1>
          <ul className="list-unstyled mt-3 mb-4">
            { this.props.txt }
          </ul>
          <Button type='button' text={ this.props.btnText } buttonClass={ this.props.btnClass} />
        </div>
      </div>
    )
  }
}