import React from "react";
import Info from './Info';
import Pricing from './Pricing';

function MainContent() {
	const freePricingData = 
    <ul className="list-unstyled">
      <li>10 users included</li>
      <li>2 GB of storage</li>
      <li>Email support</li>
      <li>Help center access</li>
    </ul>;

  	const enterprisePricingData = 
    <ul className="list-unstyled">
      <li>20 users included</li>
      <li>10 GB of storage</li>
      <li>Priority email support</li>
      <li>Help center access</li>
    </ul>;

  	const proPricingData = 
    <ul className="list-unstyled">
      <li>30 users included</li>
      <li>15 GB of storage</li>
      <li>Phone and email support</li>
      <li>Help center access</li>
    </ul>;  

    return (
    	<div className="main-content">
	      <Info />
	      <div className="container">
	        <div className="card-deck pricing mb-3 text-center">
	          <Pricing title='Free' charge='$0' txt={ freePricingData } btnClass="btn-lg btn-block btn-outline-primary" btnText='Sign up for free' />
	          <Pricing title='Pro' charge='$15' txt={ enterprisePricingData } btnClass="btn-lg btn-block btn-primary" btnText='Get started' />
	          <Pricing title='Enterprise' charge='$29' txt={ proPricingData } btnClass="btn-lg btn-block btn-primary" btnText='Contact us' />
	        </div>
	      </div>
	    </div>
    );
}

export default MainContent;