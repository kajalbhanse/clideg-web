import React, { Component } from 'react';
import Button from './Button';

export default class FormList extends Component{
	constructor(props){
		super(props);

		this.state = {
      fields: {},
      errors: {}
    }
	}

	handleValidation(){
    let fields = this.state.fields;
    let errors = {};
    let formIsValid = true;

    //Name
    if(!fields["username"]){
      formIsValid = false;
      errors["username"] = "Please Enter Your User Name";
    }

    //Email
    if(!fields["email"]){
      formIsValid = false;
      errors["email"] = "Please Enter Email";
    }

    if(typeof fields["email"] !== "undefined"){
      let lastAtPos = fields["email"].lastIndexOf('@');
      let lastDotPos = fields["email"].lastIndexOf('.');

      if (!(lastAtPos < lastDotPos && lastAtPos > 0 && fields["email"].indexOf('@@') === -1 && lastDotPos > 2 && (fields["email"].length - lastDotPos) > 2)) {
        formIsValid = false;
        errors["email"] = "Entered Email is not valid";
      }
    }

    //Password
    if(!fields["password"]){
      formIsValid = false;
      errors["password"] = "Please Enter Password";
    }

    if (typeof fields["password"] !== "undefined") {
      if (!fields["password"].match(/^.*(?=.{8,})(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[@#$%&]).*$/)) {
        formIsValid = false;
        errors["password"] = "Make sure it's at least 8 characters including a number, one special character, a capital and a lowercase letter.";
      }
    }

    this.setState({errors: errors});
    return formIsValid;
  }

  loginSubmit(e){
    e.preventDefault();
    if(this.handleValidation()){
    	const data = this.state.fields;
		  console.log(data);
      alert("Form submitted");
    }
  }

  handleChange(field, e){    		
    let fields = this.state.fields;
    fields[field] = e.target.value;        
    this.setState({fields});
  }

	render(){
		return(
			<form name="loginform" className="loginform" onSubmit= {this.loginSubmit.bind(this)} noValidate>
				<div className="form-group">
					<label>User Name</label>
					<input name="username" type="text" className="form-control" placeholder="Enter User Name" onChange={this.handleChange.bind(this, "username")} value={this.state.fields.username || ''}/>
					<span className="error">{this.state.errors["username"]}</span>
				</div>
				<div className="form-group">
					<label>Email</label>
					<input name="email" type="email" className="form-control" placeholder="Enter Email" onChange={this.handleChange.bind(this, "email")} value={this.state.fields.email || ''}/>
					<span className="error">{this.state.errors["email"]}</span>
				</div>
				<div className="form-group">
					<label>Password</label>
					<input name="password" type="password" className="form-control" placeholder="Enter Password" onChange={this.handleChange.bind(this, "password")} value={this.state.fields.password || ''}/>
					<span className="error">{this.state.errors["password"]}</span>
				</div>
				<div className="form-group">
					<Button type='submit' text='Submit' id='submit' buttonClass='btn-outline-primary'/>
				</div>
			</form>
		);
	}
} 