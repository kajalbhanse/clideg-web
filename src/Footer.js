import React, { Component } from "react";
import flogo from './flogo.svg';

class Footer extends Component{
	render(){
	    return (
	    	<footer className="pt-4 my-md-5 pt-md-5 border-top">
		    	<div className="container">
				    <div className="row">
				      	<div className="col-12 col-md">
				      		<img src={flogo} className="footer-logo" alt="logo" />
		        			<small className="d-block mb-3 text-muted">&copy; 2017-2019</small>
				      	</div>
				      	<div className="col-6 col-md">
					        <h5 className="text-left">Features</h5>
					        <ul className="list-unstyled text-small text-left">
					          <li><a className="text-muted" href="#">Cool stuff</a></li>
					          <li><a className="text-muted" href="#">Random feature</a></li>
					          <li><a className="text-muted" href="#">Team feature</a></li>
					          <li><a className="text-muted" href="#">Stuff for developers</a></li>
					          <li><a className="text-muted" href="#">Another one</a></li>
					          <li><a className="text-muted" href="#">Last time</a></li>
					        </ul>
					    </div>
					    <div className="col-6 col-md">
					        <h5 className="text-left">Resources</h5>
					        <ul className="list-unstyled text-small text-left">
					          <li><a className="text-muted" href="#">Resource</a></li>
					          <li><a className="text-muted" href="#">Resource name</a></li>
					          <li><a className="text-muted" href="#">Another resource</a></li>
					          <li><a className="text-muted" href="#">Final resource</a></li>
					        </ul>
					    </div>
					    <div className="col-6 col-md">
					        <h5 className="text-left">About</h5>
					        <ul className="list-unstyled text-small text-left">
					          <li><a className="text-muted" href="#">Team</a></li>
					          <li><a className="text-muted" href="#">Locations</a></li>
					          <li><a className="text-muted" href="#">Privacy</a></li>
					          <li><a className="text-muted" href="#">Terms</a></li>
					        </ul>
					    </div>
				    </div>
				</div>
			</footer>
	    );
	}
}

export default Footer;