import React, { Component } from "react";
import Button from './Button';
import Popup from './Popup';

export default class ListPhoto extends Component {
  constructor(props) {
    super(props);

    this.state = {
      modalVisible: false,
      photo: []
    };
    this.openModal = this.openModal.bind(this);
  }

  openModal() {
    const modalVisible = !this.state.modalVisible;
    console.log(modalVisible);
    this.setState({
      modalVisible
    });
  }

  componentDidMount() {
   fetch('https://jsonplaceholder.typicode.com/photos')
    .then(response => response.json())
    .then(photo => this.setState({photo}))
  }

  render() {
    const photos = this.state.photo.slice(0, 3).map((item, i) => (
      <div className="col-sm-4">
	      <div className="card">
          <img className='image' alt='none' src={ item.thumbnailUrl } />
	        <h5 className="card-title">{ item.title }</h5>
	        <span>Id No:- { item.id }</span>
	        <p>{ item.url}</p>
          <button
          type="button"
          onClick={this.openModal}
          className="btn btn-info btn-lg"
        >
          Open Modal
        </button>
	   	  </div>
      </div>
    ));

    return (
      <div className="jumbotron">
      	<div className="container">
      		<h1>Data Fetch from API with Image</h1>
	        <div className="panel-list row">
	        	{ photos }
	        </div>
          {
            this.state.modalVisible ? 
              <Popup modalVisible={this.state.modalVisible} modalClose={this.openModal}/> : null
          }
          
	    </div>
      </div>
    );
  }
}