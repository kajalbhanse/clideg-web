import React, { Component } from "react";
import Button from './Button';

export default class CartList extends Component {
  constructor(props) {
    super(props);

    this.state = {cart: []};
  }

  componentDidMount() {
   	fetch('https://jsonplaceholder.typicode.com/posts')
  		.then(response => response.json())
  		.then(cart => this.setState({cart}))
  }

  render() {
    const carts = this.state.cart.slice(0, 3).map((item, i) => (
      <div className="col-sm-4">
	      <div className="card">
	        <h5 className="card-title">{ item.title }</h5>
	        <span>Title No:- { item.id }</span>
	        <p>{ item.body}</p>
	        <Button type='button' text='Click here to Launch Popup' buttonClass='btn-outline-primary' />
	   	</div>
      </div>
    ));

    return (
      <div className="jumbotron">
      	<div className="container">
      		<h1>Data Fetch from API</h1>
	        <div className="panel-list row">
	        	{ carts }
	        </div>
	    </div>
      </div>
    );
  }
}