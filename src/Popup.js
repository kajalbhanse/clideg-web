import React, { Component } from "react";

export default class Modal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false
    };
    this.closeModal = this.closeModal.bind(this);
  }

  componentDidMount(){
    this.setState({
      modalVisible: this.props.modalVisible
    })
  }

  closeModal(){
    this.props.modalClose();
  }

  render() {
    let styles = this.state.modalVisible
      ? { display: "block", opacity: 1 }
      : { display: "none" };
    return (
      <div className="modal-box">
        <div
          className="modal" id="myModal"
          style={styles}
        >
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">Modal Header</h4>
                <button
                  type="button"
                  onClick={this.closeModal}
                  className="close"
                >
                  &times;
                </button>
              </div>
              <div className="modal-body">
                <p>Some text in the modal.</p>
              </div>
              <div className="modal-footer">
                <button
                  onClick={this.closeModal}
                  type="button"
                  className="btn btn-default"
                >
                  Close
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}