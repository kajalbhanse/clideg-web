import React, { Component } from 'react';
import FormBox from './FormBox';
import Popup from './Popup';

export default class FormDisplay extends Component{
	render(){
		return(
			<div className="login-sec">
				<div className="container">
					<div className="row">
						<div className="col-sm-6">
							<div className="form-ui">
								<h2 className="loginHead">Login Form</h2>
								<FormBox />
							</div>
						</div>
						<div className="col-sm-6">
							<Popup />
						</div>
					</div>
				</div>
			</div>
		);
	}
}