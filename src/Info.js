import React from "react";

function Info() {
	return(
		<div className="pricing-header mx-auto text-center">
  			<h1 className="display-4">Pricing</h1>
  			<p className="lead">Quickly build an effective pricing table for your potential customers with this Bootstrap example. It’s built with default Bootstrap components and utilities with little customization.</p>
		</div>
	);
}

export default Info;