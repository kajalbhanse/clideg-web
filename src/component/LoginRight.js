import React, { Component } from "react";
import LoginForm from './LoginForm';
import ForgotPassword from './ForgotPassword';
import ResetPassword from './ResetPassword';

export default class LoginRight extends Component{
  render() {
    return (
      <div className="lright-wrapper">
        <div className="client-info">
          <div className="client-img">
            <img src={require('./../images/client-logo.png')} />
          </div>
          <div className="client-para">
            <h4>Headline of TOI about safety</h4>
            <p>People who put themselves on the line and sacrifice their own safety for the greater good and for others.</p>
          </div>
        </div>
        <div className="login-bottom">
          <LoginForm />
          {/* <ForgotPassword /> */}
          {/* <ResetPassword /> */}
        </div>
      </div>
    )
  }
}