import React, { Component } from "react";
import * as Icon from 'react-feather';
import 'font-awesome/css/font-awesome.min.css';

export default class LoginLeftBottom extends Component{
  render() {
    return (
      <div className="lbottom-wrapper">
        <div className="clide-logo">
          <img className="inner-logo" src={require('./../images/clideg-logo.png')} />
        </div>
        <div className="bottom-text">
          <h4>Safety is our No1 Priority</h4>
          <p>People who put themselves on the line and sacrifice their own safety for the greater good and for others, and anyone in any profession whose concern is the People who put themselves on the line and sacrifice their own safety for the greater good and for others, and anyone in any profession whose concern is the </p>
        </div>
        <div className="clide-text">
          <h6>CLIDE E-learning</h6>
          <h6 className="mid-text">CLIDE Analyser</h6>
          <h6>CLIDE Group</h6>
        </div>
        <div className="social-box">
          <i className="fa fa-linkedin social-icon" aria-hidden="true"></i>
          <i className="fa fa-twitter social-icon" aria-hidden="true"></i>
          <i className="fa fa-facebook social-icon" aria-hidden="true"></i>
        </div>
      </div>
    )
  }
}