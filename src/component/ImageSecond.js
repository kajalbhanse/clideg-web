import React, { Component } from "react";

export default class ImageSecond extends Component{
  render() {
    return (
      <div className="rimage-second">
        <div className="image-box1">
          <img src={require('./../images/evd1.jpg')} />
        </div>
        <div className="image-box1">
          <img className="img-second" src={require('./../images/evd2.jpg')} />
        </div>
      </div>
    )
  }
}