import React, { Component } from "react";
import { Button } from 'react-bootstrap';

export default class LoginForm extends Component{
  constructor(props) {
    super(props);
    
    this.state = {
      className: "",
      className1: "",
      email: 'User Name',
      password: 'Password'
    };
    
    this.handleInputEmail = this.handleInputEmail.bind(this);
    this.handleInputPassword = this.handleInputPassword.bind(this);
  }
  
  handleInputEmail(evt) {
    if(evt.target.value !== '') {
      this.setState({className: "on"});
    }
    else {
      this.setState({className: ""});
    }
  }

  handleInputPassword(evt) {
    if(evt.target.value !== '') {
      this.setState({className1: "on"});
    }
    else {
      this.setState({className1: ""});
    }
  }

  render() {
    return (
      <div className="login-form">
        <h4>LOGIN</h4>
        <form>
          <div className="floatingLabelInput">
            <label className={this.state.className}>{this.state.email}</label>
            <input type="email" placeholder={this.state.email} onChange={this.handleInputEmail} />
          </div>
          <div className="floatingLabelInput">
            <label className={this.state.className1}>{this.state.password}</label>
            <input type="password" placeholder={this.state.password} onChange={this.handleInputPassword} />
          </div>
        </form>
        <div className="forget-div">
          <a href="#">Forgot Password</a>
        </div>
        <div className="login-btn">
          <Button variant="outline-primary" className="red-btn">LOGIN</Button>
        </div>
      </div>
    )
  }
}