import React, { Component } from "react";

export default class ImageThird extends Component{
  render() {
    return (
      <div className="rimage-third">
        <div className="third-box1">
          <img src={require('./../images/evd1.jpg')} />
        </div>
        <div className="third-box2">
          <div className="image-box3">
            <img src={require('./../images/evd2.jpg')} />
          </div>
          <div className="image-box3 image-last">
            <img src={require('./../images/evd3.jpg')} />
            <h6>+2 more</h6>
          </div>
        </div>
      </div>
    )
  }
}