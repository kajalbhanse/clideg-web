import React, { Component } from "react";
import { Button } from 'react-bootstrap';

export default class ResetPassword extends Component{
  constructor(props) {
    super(props);
    
    this.state = {
      className: "",
      className1: "",
      newPassword: 'New Password',
      confirmNewPassword: 'Confirm Password'
    };
    
    this.handleInputNewPass = this.handleInputNewPass.bind(this);
    this.handleInputConfirmNewPass = this.handleInputConfirmNewPass.bind(this);
  }

  handleInputNewPass(evt) {
    if(evt.target.value !== '') {
      this.setState({className: "on"});
    }
    else {
      this.setState({className: ""});
    }
  }

  handleInputConfirmNewPass(evt) {
    if(evt.target.value !== '') {
      this.setState({className1: "on"});
    }
    else {
      this.setState({className1: ""});
    }
  }

  render() {
    return (
      <div className="login-form">
        <h4>Reset Password</h4>
        <form>
          <div className="floatingLabelInput">
            <label className={this.state.className}>{this.state.newPassword}</label>
            <input type="password" placeholder={this.state.newPassword} onChange={this.handleInputNewPass} />
          </div>
          <div className="floatingLabelInput">
            <label className={this.state.className1}>{this.state.confirmNewPassword}</label>
            <input type="password" placeholder={this.state.confirmNewPassword} onChange={this.handleInputConfirmNewPass} />
          </div>
        </form>
        <div className="login-btn">
          <Button variant="outline-primary" className="red-btn">SUBMIT</Button>
        </div>
      </div>
    )
  }
}