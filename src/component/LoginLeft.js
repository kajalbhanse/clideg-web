import React, { Component } from "react";
import LoginLeftBottom from './LoginLeftBottom';

export default class LoginLeft extends Component{
  render() {
    return (
      <div className="lleft-wrapper">
        <div className="lleft-gif">
          <img src={require('./../images/clide-gif.gif')} />
        </div>
        <div className="lright-bottom">
          <LoginLeftBottom />
        </div>
      </div>
    )
  }
}