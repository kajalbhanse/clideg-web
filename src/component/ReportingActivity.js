import React, { Component } from "react";
import ImageOne from "./ImageOne";
import ImageSecond from "./ImageSecond";
import ImageThird from "./ImageThird";
import { Button, Modal } from 'react-bootstrap';
import { Image } from 'react-feather';

export default class ReportingActivity extends Component{
  constructor(props){
    super(props);
    this.handleClose = this.handleClose.bind(this);
    this.handleShow = this.handleShow.bind(this);
    this.state = {
      show: false
    }
  }

  handleClose(){
    this.setState({
      show:false
    });
  }

  handleShow(){
    this.setState({
      show:true
    });
  }

  render() {
    return (
      <div>
      <div className="reporting-box">
        <div className="report-top">
          <div className="image-box">
            <img src={require('./../images/profile.jpg')} />
          </div>
          <div className="list">
            <h5>Vijay Mahale</h5>
            <h6>Mysore 2 <span className="divider">|</span> <span className="project-name">South 1</span></h6>
            <h6 className="overdue-head">Overdue <span className="divider">|</span> <span className="days">14 days</span></h6>
          </div>
          <div className="severity">
            <div className="severity-box"></div>
          </div>
        </div>
        <div className="report-middle">
          <div className="middle-left">
            <h6>Activity <span className="divider">|</span> <span className="inside">Concrete Pump Operation </span></h6>
            <h6>Hazard <span className="divider">|</span> <span className="inside">Concrete Waste </span></h6>
            <h6>Observation <span className="divider">|</span> <span className="inside">observation</span></h6>
          </div>
          <div className="middle-right">
            <h6>Similar Act <span className="divider">|</span> <span className="days">3</span> </h6>
          </div>
        </div>
        <div className="report-images">
          <ImageOne />
        </div>
        <div className="resolve-box">
          <Button variant="outline-primary" className="resolve-btn" onClick={this.handleShow}>RESOLVE</Button>
        </div>
      </div>
      <div className="reporting-box">
        <div className="report-top">
          <div className="image-box">
            <img src={require('./../images/profile.jpg')} />
          </div>
          <div className="list">
            <h5>Vijay Mahale</h5>
            <h6>Mysore 2 <span className="divider">|</span> <span className="project-name">South 1</span></h6>
            <h6 className="overdue-head">Overdue <span className="divider">|</span> <span className="days">14 days</span></h6>
          </div>
          <div className="severity">
            <div className="severity-box"></div>
          </div>
        </div>
        <div className="report-middle">
          <div className="middle-left">
            <h6>Activity <span className="divider">|</span> <span className="inside">Concrete Pump Operation </span></h6>
            <h6>Hazard <span className="divider">|</span> <span className="inside">Concrete Waste </span></h6>
            <h6>Observation <span className="divider">|</span> <span className="inside">observation</span></h6>
          </div>
          <div className="middle-right">
            <h6>Similar Act <span className="divider">|</span> <span className="days">3</span> </h6>
          </div>
        </div>
        <div className="report-images">
          <ImageSecond />
        </div>
      </div>
      <div className="reporting-box">
        <div className="report-top">
          <div className="image-box">
            <img src={require('./../images/profile.jpg')} />
          </div>
          <div className="list">
            <h5>Vijay Mahale</h5>
            <h6>Mysore 2 <span className="divider">|</span> <span className="project-name">South 1</span></h6>
            <h6 className="overdue-head">Overdue <span className="divider">|</span> <span className="days">14 days</span></h6>
          </div>
          <div className="severity">
            <div className="severity-box"></div>
          </div>
        </div>
        <div className="report-middle">
          <div className="middle-left">
            <h6>Activity <span className="divider">|</span> <span className="inside">Concrete Pump Operation </span></h6>
            <h6>Hazard <span className="divider">|</span> <span className="inside">Concrete Waste </span></h6>
            <h6>Observation <span className="divider">|</span> <span className="inside">observation</span></h6>
          </div>
          <div className="middle-right">
            <h6>Similar Act <span className="divider">|</span> <span className="days">3</span> </h6>
          </div>
        </div>
        <div className="report-images">
          <ImageThird />
        </div>
      </div>

      <Modal show={this.state.show} onHide={this.handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>RESOLVE</Modal.Title>
          <Button variant="outline-primary" className="modal-btn">DONE</Button>
        </Modal.Header>

        <Modal.Body>
          <form>
            <div className="form-group">
              <label>Closing Remark</label>
              <input type="email" className="form-control" id="email" placeholder="Enter Closing Remark" name="email" />
            </div>
            <div className="form-group">
              <label>Add Closing Evidence</label>
              <div class="box">
                <input type="file" id="file-2" class="inputfile inputfile-2" multiple="" />
                <label for="file-2">
                  <div className="image-custom">
                    <Image color='#28AED9' size="22" />
                  </div>
                  <span>Browse Images</span>
                </label>
              </div>
            </div>
          </form>
          <div className="image-preview">
            <div className="preview-box">
              <img src={require('./../images/evd2.jpg')} />
            </div>
          </div>
        </Modal.Body>
      </Modal>
    </div>
    )
  }
}