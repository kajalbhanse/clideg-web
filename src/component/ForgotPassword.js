import React, { Component } from "react";
import { Button } from 'react-bootstrap';
import { ArrowLeftCircle } from 'react-feather';

export default class ForgotPassword extends Component{
  constructor(props) {
    super(props);
    
    this.state = {
      className: "",
      email: 'Enter Your Registerd Email Id',
    };
    
    this.handleInputEmail = this.handleInputEmail.bind(this);
  }
  
  handleInputEmail(evt) {
    if(evt.target.value !== '') {
      this.setState({className: "on"});
    }
    else {
      this.setState({className: ""});
    }
  }

  render() {
    return (
      <div className="login-form">
        <h4>Forgot Password</h4>
        <form>
          <div className="floatingLabelInput">
            <label className={this.state.className}>{this.state.email}</label>
            <input type="email" placeholder={this.state.email} onChange={this.handleInputEmail} />
          </div>
        </form>
        <div className="login-btn">
          <Button variant="outline-primary" className="red-btn">SUBMIT</Button>
        </div>
        <div className="back-div">
          <ArrowLeftCircle color='#000' size="18" />
          <a href="#">Back to LOGIN</a>
        </div>
      </div>
    )
  }
}