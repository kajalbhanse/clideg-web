import React from 'react';

function Button(props) {
    return (
        <button type="{ props.type }" className={ `btn ${ props.buttonClass }` }>{ props.text }</button>
    );
}

export default Button;