import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Website from './Website';

function App() {
  return (
    <div className="App">
      <Website />
    </div>
  );
}

export default App;
